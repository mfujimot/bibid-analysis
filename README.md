Set-up
------

Package for the analysis of the Beam-Induced Background in the Inner Detector.

To start using the code, first clone the repository:

`git clone https://gitlab.cern.ch/edrossi/bibid-analysis.git`

`cd bibid-analysis`

To build the package use:

`source compile.sh`

This command is necessary only if it is the first time setting up the package, or if the source code has been modified. If the package was setup up before, use:

`source setup.sh`

To run the code, you need to be in the "run" folder. To test the code locally with an example DAOD_IDNCB (FIXME: the example file is not in a public folder right now) use:

`athena BIBIDAnalysis_topOptions.py`

To run on the grid use:

`python sendGridJob.py`

Note that in this python script you must select the right txt file with a list of the DAOD_IDNCB files to run on.

Code Output
-----------

The code runs on DAOD_IDNCB files and produces an output NTuple with a much more manageable size (for 2018 about 7Gb). The NTuple can then be processed with a simple plotting macro to produce the desired results. 

The NTuple contains the following information:

| Quantity            | Type           | Comment                                                           |
|---------------------|:--------------:|-------------------------------------------------------------------|
| EventNumber         | int            |                                                                   |
| RunNumber           | int            |                                                                   |
| TimeStamp           | int            |                                                                   |
| LBNumber            | int            |                                                                   |
| BCID                | int            |                                                                   |
| GoodLB              | bool           | true if LB is in GRL                                              |
| ACTrigger           | bool           | true if BCM AC trigger fired (ISO or NONISO)                      |
| CATrigger           | bool           | true if BCM CA trigger fired (ISO or NONISO)                      |
| isoTrigger          | bool           | true if BCM trigger is ISO                                        |
| BCMprescale         | float          | prescale of the BCM trigger                                       |
| HLTSCTTrigger       | bool           | true if HLT_mb_sp_ncb_L1RD0_UNPAIRED_ISO fired                    |
| HLTSCTprescale      | float          | prescale of HLT sp ncb trigger                                    |
| isBeam1             | bool           | true if beam 1 is filled in LHC fill parameter                    |
| isBeam2             | bool           | true if beam 2 is filled in LHC fill parameter                    |
| isUnpaired          | bool           | true if bunch is filled in LHC fill parameter                     |
| gapOppositeBefore   | int            | BCID distance to previous filled bunch in opposite direction      |
| gapOppositeAfter    | int            | BCID distance to next filled bunch in opposite direction          |
| gapSameBefore       | int            | BCID distance to previous filled bunch in same direction          |
| gapSameAfter        | int            | BCID distance to next filled bunch in same direction              |
| IntensityBeam       | float          | bunch intensity of the filled beam (in 10^11 protons)             |
| VertexTracks        | vector(int)    | number of tracks associated to each vertex                        |
| VertexType          | vector(int)    | type of each vertex (1 = primary, 3 = pile up)                    |
| VertexChi2          | vector(float)  | chi2 of each vertex                                               |
| VertexDoF           | vector(float)  | number of degrees of freedom of each vertex                       |
| VertexX             | vector(float)  | x position of each vertex                                         |
| VertexY             | vector(float)  | y position of each vertex                                         |
| VertexZ             | vector(float)  | z position of each vertex                                         |
| VertexpT2           | vector(float)  | pT2 of each vertex                                                |
| BCMchan             | vector(int)    | channel number of every BCM hit                                   |
| BCMpulse1Pos        | vector(int)    | position (in time) of every BCM hit                               |
| BCMpulse1Width      | vector(int)    | amplitude of every BCM hit                                        |
| BCMpulse2Pos        | vector(int)    | if present, position (in time) of second BCM hit on a channel     |
| BCMpulse2Width      | vector(int)    | if present, width of second BCM hit on a channel                  |
| BCMBCID             | vector(int)    | BCID of every BCM hit                                             |
| PixelHitsOnDisks    | vector(int)    | number of hits on each pixel EC disk (starting from ECC, 6 total) |
| SCTHitsOnDisks      | vector(int)    | number of hits on each SCT EC disk (starting from ECC, 18 total)  |
| SCTRDO OnDisks      | vector(int)    | number of RDOs on each SCT EC disk (starting from ECC, 18 total)  |
| TRTHitsOnDisks      | vector(int)    | number of hits on each TRT EC disk (starting from ECC, 28 total)  |

Process Grid output
-------------------

The sendGridJob.py script generates a list in the folder "tools/gridDownloader" with the name of all the output files expected. Once the grid jobs are finished, it is possible to download the output files by using the script in the "tools/gridDownloader" folder:

`source downloader.sh`

This script will download all the files listed in the latest list, and merge all the root files in a single root file that can be easily processed by a plotting macro.

Plotting Macro
--------------

In order to create the final plots, it is necessary to run over the final root files with a plotting macro. An example plotting macro is in "tools/PlottingMacro". To use it, first open the file with root:

`root path/to/file.root`

Then, in the root terminal:

`TTree *t = (TTree*) _file0->Get("BIBIDAna")`

`t->Process("plottingID.C")`

The plots will be saved in the "Plots" folder and in a root file called "histogram_output.root"

To Do
-----

* Add plotting in athena code (write another src or put it in BIBIDAnalysis.cxx)
* Clean athena dependencies for compilation
* Migrate to release 22 (eventually)
