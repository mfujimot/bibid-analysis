/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/


#ifndef BIB_ID_ANALYSIS_H 
#define BIB_ID_ANALYSIS_H 

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandleKey.h"

#include "xAODEventInfo/EventInfo.h"
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"
#include "TrigAnalysisInterfaces/IBunchCrossingConfProvider.h"
#include "LumiBlockComps/ILuminosityTool.h"
#include "xAODTracking/VertexContainer.h"
#include "InDetBCM_RawData/BCM_RDO_Container.h"
#include "InDetBCM_RawData/BCM_RDO_Collection.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"

#include <string>
#include <vector>
#include "TH1.h"

class TTree;
class TH1;

class BIBIDAnalysis : public AthAlgorithm {

public:
  BIBIDAnalysis(const std::string& name, ISvcLocator* pSvcLocator);
  ~BIBIDAnalysis(){}

  virtual StatusCode initialize() override final;
  virtual StatusCode execute() override final;
  virtual StatusCode finalize() override final;

private: 
  // EventInfo
  int m_EventNumber;
  int m_RunNumber;
  int m_TimeStamp;
  int m_LBNumber;
  int m_BCID;
  
  // GoodRunsList
  bool m_GRL;
  
  // Trigger
  bool m_ACTrigger;
  bool m_CATrigger;
  bool m_isoTrigger;
  float m_BCMprescale;
  bool m_HLTSCTTrigger;
  float m_HLTSCTprescale;
  
  // Fill Parameters
  bool m_isBeam1;
  bool m_isBeam2;
  bool m_isUnpaired;
  int m_gapOppositeBefore;
  int m_gapOppositeAfter;
  int m_gapSameBefore;
  int m_gapSameAfter;
  float m_intensityBeam;
  float m_LBDuration;
  
  // Primary Vertices
  std::vector<int>* m_VertexTracks;
  std::vector<int>* m_VertexType;
  std::vector<float>* m_VertexChi2;
  std::vector<float>* m_VertexDoF;
  std::vector<float>* m_VertexX;
  std::vector<float>* m_VertexY;
  std::vector<float>* m_VertexZ;
  std::vector<float>* m_VertexpT2;
  
  // BCM_RDO
  std::vector<int>* m_BCMchan;
  std::vector<int>* m_BCMpulse1Pos;
  std::vector<int>* m_BCMpulse1Width;
  std::vector<int>* m_BCMpulse2Pos;
  std::vector<int>* m_BCMpulse2Width;
  std::vector<int>* m_BCMBCID;
  
  // Pixel_Cluster
  std::vector<int>* m_PixelHitsOnDisks; 
  std::vector<int>* m_PixelRDOOnDisks; 
  
  // SCT_Cluster
  std::vector<int>* m_SCTHitsOnDisks; 
  std::vector<int>* m_SCTRDOOnDisks; 

  //TRT_DriftCircle
  std::vector<int>* m_TRTHitsOnDisks;
  
  std::vector<int> m_confBCID;
  std::vector<float> m_bcIntensities;
  
  TTree* m_tree;
  std::string m_ntupleFileName;
  std::string m_ntupleTreeName;
  std::string m_path;
  ServiceHandle<ITHistSvc> m_thistSvc;
  ToolHandle<Trig::TrigDecisionTool> m_trigDecTool;
  ToolHandle< IGoodRunsListSelectionTool > m_GRLTool;
  ToolHandle<Trig::IBunchCrossingTool> m_bunchCrossingTool;
  ToolHandle< Trig::IBunchCrossingConfProvider > m_bcConf;
  ToolHandle<ILuminosityTool> m_lumiTool;
};

#endif // BIB_ID_ANALYSIS_H 
