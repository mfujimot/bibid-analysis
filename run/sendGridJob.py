import sys
import os
import subprocess

date_tag=subprocess.check_output('date +"%y%m%d%H%M"', shell=True).rstrip('\n')
user=subprocess.check_output('whoami', shell=True).rstrip('\n')

fileList = open("TestGrid.txt", "r")
Lines = fileList.readlines()
num = 0

TestAreaPath = os.getenv("TestArea")
DownloadFile = TestAreaPath + "/tools/gridDownloader/List" + date_tag + ".txt" 
downloadFileList = open(DownloadFile, "w")

for line in Lines:
  num = num + 1
  runNumber = line.rstrip("\n").split(".")[1]
  outDS = "user." + user + "." + "BIBIDAnalysis" + "." + runNumber + "." + date_tag
  cmd = "pathena BIBIDAnalysis_topOptions.py --inDS=" + line.rstrip("\n") + " --outDS=" + outDS 
  print "Submitting " + str(num) + "/" + str(len(Lines)) + " (" + str(round((num + 0.0)/len(Lines)*100,1)) + "%)"
  print cmd
  downloadFileList.write("user." + user + ":" + outDS + "_BIBIDAnalysis\n")
  
  subprocess.call(cmd, shell = True)

fileList.close()
downloadFileList.close()
