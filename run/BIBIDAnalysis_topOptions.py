from AthenaCommon.Constants import *
from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

ServiceMgr.AuditorSvc.Auditors += ["ChronoAuditor"]

AthenaPoolCnvSvc = Service("AthenaPoolCnvSvc")
AthenaPoolCnvSvc.UseDetailChronoStat = TRUE

from PartPropSvc.PartPropSvcConf import PartPropSvc

include("EventAthenaPool/EventAthenaPool_joboptions.py")

#----------------------------
# Input Dataset
#----------------------------
import os
from glob import glob
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
athenaCommonFlags.FilesInput = ['/afs/cern.ch/work/e/edrossi/BIBFileExample/data18_13TeV.00349592.physics_Background.merge.DAOD_IDNCB.f929_m1958._lb0142-lb0144._0001.1']
ServiceMgr.EventSelector.InputCollections = athenaCommonFlags.FilesInput()

#theApp.EvtMax = -1
theApp.EvtMax = 1000

#set up the conddb with the correct conditions tag
from IOVDbSvc.CondDB import conddb
from AthenaCommon.GlobalFlags import globalflags
conddb.setGlobalTag(globalflags.ConditionsTag())

#----------------------------
# Message Service
#----------------------------
# set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL)
ServiceMgr.MessageSvc.OutputLevel = INFO
ServiceMgr.MessageSvc.defaultLimit = 9999999

#----------------------------
# Algorithms
#----------------------------
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()
from BIBIDAnalysis.BIBIDAnalysisConf import BIBIDAnalysis
topSequence += BIBIDAnalysis()

BIBIDAnalysis = BIBIDAnalysis()
BIBIDAnalysis.NtupleFileName = '/BIBIDAnalysis/'
BIBIDAnalysis.HistPath = '/BIBIDAnalysis/'

#----------------------------
# Histogram and Tree Service
#----------------------------
from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()
ServiceMgr.THistSvc.Output += ["BIBIDAnalysis DATAFILE='BIBIDAnalysis.root' OPT='RECREATE'"]

# GoodRunList CHECK FOR UPDATES OF THE GRLs
from AthenaCommon import CfgMgr
ToolSvc += CfgMgr.GoodRunsListSelectionTool( "GRLTool" , GoodRunsListVec = [ 
"BIBIDAnalysis/data15_13TeV.periodAllYear_HEAD_Unknown_PHYS_StandardGRL_All_Good.xml",
"BIBIDAnalysis/data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",
"BIBIDAnalysis/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
"BIBIDAnalysis/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"] )

#globalflags.DataSource.set_Value_and_Lock('data')
conddb.dbname = "CONDBR2"
import IOVDbSvc.IOVDb 
IOVDbSvc = Service( "IOVDbSvc" )
#include("IOVDbSvc/IOVDbSvc_jobOptions.py")
IOVDbSvc.DBInstance=""
from TrigBunchCrossingTool.BunchCrossingTool import BunchCrossingTool
ToolSvc += BunchCrossingTool("LHC")

from TrigBunchCrossingTool.BunchCrossingConfProvider import BunchCrossingConfProvider
ToolSvc += BunchCrossingConfProvider("LHC")
# For bunch intensity: IntensityChannel = 0 for BPTX and = 1 for BCT
ToolSvc.BunchCrossingTool.IntensityChannel = 1

conddb.isMC = 0
rec.doExpressProcessing = 1
from LumiBlockComps.LuminosityToolDefault import LuminosityToolDefault
theLumiTool = LuminosityToolDefault()
ToolSvc += theLumiTool
