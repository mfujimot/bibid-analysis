setupATLAS

asetup Athena,21.0,21.0.122

cd build

rm -rf *

cmake ../source

make -j

source */setup.sh
cd ../run

lsetup panda

voms-proxy-init -voms atlas
